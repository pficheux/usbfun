 *** LED message board driver ***

 use /sys/bus/usb/driver/<id>/text to display a message.
 ex: echo -n "kikou" > /sys/bus/usb/drivers/msg_board/3-2:1.0/text

 use /sys/module/msg_board/parameters/sleepms to select the display speed.
 ex: echo -n 300 > /sys/module/msg_board/parameters/sleepms

 use /sys/module/msg_board/parameters/disptype to select the display type.
 		 0 = fixed
		 1 = scroll left
		 2 = scroll right
