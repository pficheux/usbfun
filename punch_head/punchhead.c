#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <asm/uaccess.h>

#define UNLOCK_IOCTL 0


MODULE_AUTHOR("Jerome Kurtz, Paul-Marie Raffi, Vincent Monloys");
MODULE_DESCRIPTION("Punch head driver");
MODULE_LICENSE("GPL");

#define VENDOR_ID	0x04f2
#define PRODUCT_ID	0x0111


#define buff_size 1024
//static int major = 0;
static size_t real_size = 0;
static char mybuff[buff_size];

// Private structure
struct usb_punchhead {
  struct usb_device *	udev;
  uint64_t		button;
};

// Forward declaration 
static struct usb_driver punchhead_driver;

/* Table of devices that work with this driver */
static struct usb_device_id id_table [] = {
	{ USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
	{ },
};
MODULE_DEVICE_TABLE (usb, id_table);

/* Ask panic button for button status */
static int get_punchhead_button_status (struct usb_punchhead *punchhead_dev)
{
  char *buf;
  int actual_len = -115;
  int ret = 0;
  int i = 3;
  int j = 0;

  printk (KERN_INFO "get_punchhead_button_status\n");

  // Allocate msg buffer
  if (!(buf = kmalloc(8, GFP_KERNEL))) {
    printk(KERN_WARNING "punchhead: can't alloc buf\n");
    return -1;
  }

  memset (buf, 0, 8);
  ret = usb_interrupt_msg (punchhead_dev->udev, usb_rcvintpipe (punchhead_dev->udev, 3), buf, 8, &actual_len, 15 * HZ);
  if (ret < 0) 
    printk (KERN_WARNING "punchhead: IN, ret = %d\n", ret);
  else
  {
    punchhead_dev->button = 0;
    while (j < 2)
    {  
      while (i >= 0)
      {
	punchhead_dev->button = 0x100 * punchhead_dev->button;
	punchhead_dev->button += buf[i + j * 4] ;
	i--;
      }
      i = 3;
      j++;
    }
  }
  
  printk (KERN_INFO "0x%08x | 0x%08x | 0x%08x | 0x%08x\n", (int)(*buf), (int)(buf[1]), (int)(buf[2]), (int)(buf[3]));
  printk (KERN_INFO "0x%08x\n", punchhead_dev->button);
  kfree (buf);

  return 0;
}

/*
 * /sys functions
 */
static ssize_t show_button(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct usb_interface *intf = to_usb_interface(dev); 
  struct usb_punchhead *punchhead_dev = usb_get_intfdata(intf);

  get_punchhead_button_status (punchhead_dev);

  return sprintf(buf, "%d\n", punchhead_dev->button); 
}


static ssize_t set_button(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	return count;
}
static DEVICE_ATTR(button, 0444, show_button, set_button);


// Char device functions

static int punchhead_open (struct inode *inode, struct file *file)
{
  struct usb_punchhead *dev;
  struct usb_interface *interface;
  int minor;
  
  minor = iminor(inode);

  // Get interface for device
  interface = usb_find_interface (&punchhead_driver, minor);
  if (!interface)
    return -ENODEV;

  // Get private data from interface
  dev = usb_get_intfdata (interface);
  if (dev == NULL) {
      printk (KERN_WARNING "punchhead: can't find device for minor %d\n", minor);
      return -ENODEV;
  }

  // Set to file structure
  file->private_data = (void *)dev;

  return 0;
}

static ssize_t punchhead_read(struct file *file, char* buffer,
			 size_t length, loff_t *ppos)
{
  size_t size = (length > real_size ? real_size : length);

  if (size > 0)
  {
    copy_to_user(buffer, mybuff, size);
    real_size = 0;
    printk(KERN_INFO "Read success!\n");
    return size;
  }

  return 0;
}

static ssize_t punchhead_write(struct file *file, const char* buffer,
			  size_t length, loff_t *ppos)
{
  size_t size = (length > (buff_size - real_size) ? (buff_size - real_size)
		 : length);

  if (size > 0)
  {
    if (copy_from_user(&mybuff[real_size], buffer, size))
      return -EFAULT;
    real_size += size;
    printk(KERN_INFO "Write success!\n");
    return size;
  }
    printk(KERN_INFO "Write error!\n");
  return -1;
}



static int punchhead_release (struct inode *inode, struct file *file)
{
  return 0;
}

static int punchhead_unlocked_ioctl (struct inode *inode, struct file *file, unsigned long arg)
{
  struct usb_punchhead *dev;
  unsigned int cmd;

  cmd = 0;

  printk(KERN_DEBUG "punchhead_unlocked_ioctl\n");

  /* get the dev object */
  dev = file->private_data;
  if (dev == NULL)
    return -ENODEV;

  switch (cmd) {
    case 0 :
      printk(KERN_INFO "punchhead_ioctl\n");
      if (get_punchhead_button_status (dev) == 0) {
	if (copy_to_user((void*)arg, &(dev->button), sizeof(int))) {
	  printk (KERN_WARNING "punchhead: copy_to_user error\n");
	  return -EFAULT;
	}
      }

      break;

    default :
      printk(KERN_WARNING "punchhead_ioctl(): unsupported command %d\n", cmd);
      
      return -EINVAL;
  }
  return 0;
}


static int punchhead_ioctl (struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
  struct usb_punchhead *dev;

  /* get the dev object */
  dev = file->private_data;
  if (dev == NULL)
    return -ENODEV;

  switch (cmd) {
    case 0 :
      printk(KERN_INFO "punchhead_ioctl\n");
      if (get_punchhead_button_status (dev) == 0) {
	if (copy_to_user((void*)arg, &(dev->button), sizeof(uint64_t))) {
	  printk (KERN_WARNING "punchhead: copy_to_user error\n");
	  return -EFAULT;
	}
      }

      break;

    default :
      printk(KERN_WARNING "punchhead_ioctl(): unsupported command %d\n", cmd);
      
      return -EINVAL;
  }

  return 0;
}


static struct file_operations punchhead_fops = {
  .read    = punchhead_read,
  .write   = punchhead_write,
  .open    = punchhead_open,
  .release = punchhead_release,
  .unlocked_ioctl = punchhead_unlocked_ioctl
};

// USB driver functions

static struct usb_class_driver punchhead_class_driver = {
  .name = "usb/punchhead%d",
  .fops = &punchhead_fops,
  .minor_base = 0
};

static int punchhead_probe (struct usb_interface *interface, const struct usb_device_id *id)
{
  struct usb_device *udev = interface_to_usbdev (interface);
  struct usb_punchhead *punchhead_dev;
  int ret;

  printk (KERN_INFO "punchhead_probe: starting\n");

  ret = usb_register_dev(interface, &punchhead_class_driver);
  if (ret < 0) {
    printk (KERN_WARNING "punchhead: usb_register_dev() error\n");
    return ret;
  }

  punchhead_dev = kmalloc (sizeof(struct usb_punchhead), GFP_KERNEL);
  if (punchhead_dev == NULL) {
    dev_err (&interface->dev, "Out of memory\n");
    return -ENOMEM;
  }

  // Fill private structure and save it with usb_set_intfdata
  memset (punchhead_dev, 0x00, sizeof (*punchhead_dev));
  punchhead_dev->udev = usb_get_dev(udev);
  punchhead_dev->button = 0;
  usb_set_intfdata (interface, punchhead_dev);

  // Add /sys entry
  ret = device_create_file(&interface->dev, &dev_attr_button);
  if (ret < 0) {
    printk (KERN_WARNING "punchhead: device_create_file() error\n");
    return ret;
  }

  dev_info(&interface->dev, "USB Panic Button device now attached\n");

  return 0;
}

static void punchhead_disconnect(struct usb_interface *interface)
{
  struct usb_punchhead *dev;

  dev = usb_get_intfdata (interface);
  usb_deregister_dev (interface, &punchhead_class_driver);
  usb_set_intfdata (interface, NULL);

  device_remove_file(&interface->dev, &dev_attr_button);
  usb_put_dev(dev->udev);

  kfree(dev);

  dev_info(&interface->dev, "USB Panic Button now disconnected\n");
}

static struct usb_driver punchhead_driver = {
	.name       =	"punchhead",
	.probe      =	punchhead_probe,
	.disconnect =	punchhead_disconnect,
	.id_table   =	id_table,
};

// Init & exit

static int __init usb_punchhead_init(void)
{
  int retval = 0;

  retval = usb_register(&punchhead_driver);
  if (retval)
    printk(KERN_WARNING "usb_register failed. Error number %d", retval);

  return retval;
}

static void __exit usb_punchhead_exit(void)
{
  usb_deregister(&punchhead_driver);
}

module_init (usb_punchhead_init);
module_exit (usb_punchhead_exit);
