#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#define WIDTH 800
#define HEIGHT 600

int main (void)
{
  int keep = 1;
  SDL_Event event;
  SDL_Surface *screen = NULL;
  SDL_Surface *bg = IMG_Load("background.jpg");
  SDL_Surface *paf = IMG_Load("choc.png");
  SDL_Surface *guy = IMG_Load("coyotte01.png");
  SDL_Surface *soft = IMG_Load("coyotte02.png");
  SDL_Surface *middle = IMG_Load("coyotte03.png");
  SDL_Surface *hard = IMG_Load("coyotte04.png");
  SDL_Rect pos;
  SDL_Rect pos_paf;
  
  if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
      fprintf(stderr, "SDL Init Error : %s\n", SDL_GetError());
      return (EXIT_FAILURE);
    }

  screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_HWSURFACE | SDL_RESIZABLE | SDL_DOUBLEBUF);

  if ( screen == NULL ) 
    {
      fprintf(stderr, "SetVideoMode Error : %s\n", SDL_GetError() );
      exit(EXIT_FAILURE);
    }

  SDL_WM_SetCaption("Punch Head", NULL);

  pos.x = WIDTH / 2 - 130;
  pos.y = HEIGHT / 2 - 110;
  pos_paf.x = WIDTH / 2 - 160;
  pos_paf.y = HEIGHT / 2 - 167;

  int fd = open("/dev/punchhead0", O_RDONLY);
  int64_t args;
  while ( keep )
  {
    SDL_PollEvent( &event );
    switch ( event.type )
      {
      case SDL_QUIT:
	keep = 0;
	break;
      default:
	{
	  ioctl(fd, 0, &args);

	  args = args >> 32;
	  SDL_FillRect( screen , NULL, SDL_MapRGB( screen->format, 255, 255, 255 ) );
	  SDL_BlitSurface( bg, NULL, screen, NULL);
	      
	  switch (args) 
	    {
	    case 0x001e0001:
	      SDL_BlitSurface( paf, NULL, screen, &pos_paf);
	      SDL_BlitSurface( soft, NULL, screen, &pos);
	      SDL_Flip( screen );
	      SDL_Delay( 300 );
	      break;
	    case 0x001f0001:
	      
	      SDL_BlitSurface( paf, NULL, screen, &pos_paf);
	      SDL_BlitSurface( middle, NULL, screen, &pos);
	      SDL_Flip( screen );
	      SDL_Delay( 300 );
	      break;
	    case 0x00200001:
	      SDL_BlitSurface( paf, NULL, screen, &pos_paf);
	      SDL_BlitSurface( hard, NULL, screen, &pos);
	      SDL_Flip( screen );
	      SDL_Delay( 300 );
	      break;
	    default:
	      SDL_BlitSurface( guy, NULL, screen, &pos);
	      SDL_Flip( screen );
	    }
	}
      }
  }
  SDL_FreeSurface( guy );
  SDL_FreeSurface( bg );
  SDL_FreeSurface( paf );
  SDL_Quit();
  close(fd);
  return 0;
}
