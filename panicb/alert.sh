#!/bin/sh
# Get device id in /sys
X=$(ls /sys/bus/usb/drivers/panicb)
ID=$(echo $X | cut -d" " -f1)

while [ 1 ]
do
    if [ $(cat /sys/bus/usb/drivers/panicb/${ID}/button) -ge 1 ]; then
	mplayer alert.wav
    fi

    sleep 1
done

