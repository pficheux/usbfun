/*
 * Test program for USB Webmail Notifier driver
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <limits.h>

int main (int argc, char **argv)
{
  int i;

  if (argc != 5) {
    fprintf(stderr, "uwn-test device(=/dev/uwn0) r g b\n");
    return EXIT_FAILURE;
  }

  unsigned char colors[3];

  for (i = 0; i < sizeof(colors) / sizeof(colors[0]); ++i) {
    unsigned long res = strtoul(argv[i + 2], NULL, 10);

    if (res == ULONG_MAX || res > 255) {
      fprintf(stderr, "color must be between 0 and 255.\n");
      return EXIT_FAILURE;
    }
    colors[i] = res;
  }

  int fd = open(argv[1], O_RDONLY);

  if (fd < 0) {
    perror("open()");
    return EXIT_FAILURE;
  }

  if (ioctl(fd, 0, (colors[0] << 16) | (colors[1] << 8) | colors[2]) < 0) {
    perror("ioctl()");
    close(fd);
    return EXIT_FAILURE;
  }

  close(fd);

  printf("color successfully set!\n");

  return EXIT_SUCCESS;
}
