#!/bin/sh
#
# Check mailbox with fetchmail, display result with UWN (red or green)
# 

# Get device id in /sys
X=$(ls /sys/bus/usb/drivers/uwn)
ID=$(echo $X | cut -d" " -f1)

# Get message number
N=$(fetchmail -c)

# No mail?
if [ $? -ne 0 ]; then
    echo "No mail."
    COLOR="0 0 0"
else
    # if > 10 msg set "red", else "green"
    if [ $(echo $N | cut -d" " -f1) -le 10 ]; then
	COLOR="0 255 0"
    else
	COLOR="255 0 0"
    fi
fi

# Send color to UWN
echo -n "${COLOR}" > /sys/bus/usb/drivers/uwn/${ID}/color

