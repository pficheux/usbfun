/*
 * Detach USB device from HID
 */
#include <stdio.h>
#include <stdlib.h>
#include <usb.h>
#include <string.h>
#include <errno.h>

// Let's find the device
usb_dev_handle *find_usb_device(int vendor_id, int product_id) 
{
    struct usb_bus *bus;
    struct usb_device *dev;

    for (bus = usb_busses; bus; bus = bus->next) {
      for (dev = bus->devices; dev; dev = dev->next) {
	if (dev->descriptor.idVendor == vendor_id && dev->descriptor.idProduct == product_id) {
	  struct usb_dev_handle *handle;
	  printf ("USB Device with Vendor Id: %x and Product Id: %x found.\n", vendor_id, product_id);
	  if (!(handle = usb_open (dev))) {
	    fprintf (stderr, "Could not open USB device\n");
	    return NULL;
	  }
	  
	  return handle;
	}
      }
    }
	
    return NULL;
}


int main (int ac, char **av)
{
  usb_dev_handle *dev;
  int v_id, p_id, ret;

  if (ac < 3) {
    fprintf (stderr, "Usage: %s Vendor_ID Product_ID\n", av[0]);
    exit (1);
  }

  sscanf (av[1], "%x", &v_id);
  sscanf (av[2], "%x", &p_id);

  usb_init ();
  usb_find_busses ();
  usb_find_devices ();

  if (!(dev = find_usb_device (v_id, p_id)))
    exit(1);

  /* try to detach device in case usbhid has got hold of it */
  ret = usb_detach_kernel_driver_np (dev, 0);
  if (ret != 0) {
    if (errno == ENODATA)
      printf ("Device already detached\n");
    else 
      printf ("Detach failed: %s[%d]\n", strerror (errno), errno);
  }
  else
    printf ("Detach successful\n");
}
