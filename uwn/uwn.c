/*
 * Linux USB driver for USB Webmail Notifier (DreamCheeky)
 *
 *   Copyright (C) 2010 Pierre Ficheux & EPITA/GISTR students
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/usb.h>

#define DRIVER_AUTHOR "Pierre Ficheux <pierre.ficheux@gmail.com> + EPITA/GISTR students"
#define DRIVER_DESC   "USB Webmail Notifier driver"

#define VENDOR_ID   0x1d34
#define PRODUCT_ID  0x4

struct usb_uwn {
  struct usb_device *udev;
  int r, g, b;
};


// Forward declaration 
static struct usb_driver uwn_driver;

/* Table of devices that work with this driver */
static struct usb_device_id id_table [] = {
  { USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
  {}
};
MODULE_DEVICE_TABLE (usb, id_table);

static int uwn_dev_init (struct usb_uwn *uwn_dev)
{
  int ret;
  char buf[8];

  printk(KERN_DEBUG "uwn_dev_init\n");
  memset(buf, 0, sizeof(buf));

  /* first message */
  buf[0] = 0x1f;
  buf[1] = 0x02;
  buf[3] = 0x2e;
  buf[6] = 0x2b;
  buf[7] = 0x03;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);

  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  /* second message */
  buf[0] = 0;
  buf[7] = 0x04;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);

  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  /* third message */
  memset(buf, 0, sizeof(buf));
  buf[6] = 0x2b;
  buf[7] = 0x05;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);
  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  return 0;
}

static int uwn_dev_release (struct usb_uwn *uwn_dev)
{
  int ret;
  char buf[8];

  printk (KERN_DEBUG "uwn_dev_release\n");
  memset(buf, 0, sizeof(buf));

  /* first message */
  buf[0] = 0x07;
  buf[6] = 0x2b;
  buf[7] = 0x03;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);
  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  /* second message */
  buf[7] = 0x04;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);
  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  /* third message */
  memset(buf, 0, sizeof(buf));
  buf[6] = 0x2b;
  buf[7] = 0x05;

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);
  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  return 0;
}

static int uwn_set_color (struct usb_uwn *uwn_dev, uint8_t r, uint8_t g, uint8_t b)
{
  int ret = 0;
  char buf[8];

  memset(buf, 0, sizeof(buf));

  buf[0] = r;
  buf[1] = g;
  buf[2] = b;

  buf[6] = 0x2b;
  buf[7] = 0x05;

  printk (KERN_DEBUG "uwn_set_color r=%d, g=%d, b=%d\n", r, g, b);

  ret = usb_control_msg(uwn_dev->udev, usb_sndctrlpipe(uwn_dev->udev, 0),
      0x09, USB_TYPE_CLASS | USB_RECIP_INTERFACE, 0x200, 0x00, buf, 8, 2 * HZ);

  if (ret < 0)
    printk (KERN_WARNING "uwn: IN, ret = %d\n", ret);

  return 0;
}

static int uwn_open (struct inode *inode, struct file *file)
{
  struct usb_uwn *dev;
  struct usb_interface *interface;
  int minor;
  
  minor = iminor(inode);

  // Get interface for device
  interface = usb_find_interface (&uwn_driver, minor);
  if (!interface)
    return -ENODEV;

  // Get private data from interface
  dev = usb_get_intfdata (interface);
  if (dev == NULL) {
      printk (KERN_WARNING "uwn_open(): can't find device for minor %d\n", minor);
      return -ENODEV;
  }

  // Set to file structure
  file->private_data = (void *)dev;

  return 0;
}

static int uwn_release (struct inode *inode, struct file *file)
{
  return 0;
}

static int uwn_ioctl (struct inode *inode, struct file *file,
    unsigned int cmd, unsigned long arg)
{
  struct usb_uwn *dev;

  printk(KERN_DEBUG "uwn_ioctl\n");

  /* get the dev object */
  dev = file->private_data;
  if (dev == NULL)
    return -ENODEV;

  switch (cmd) {
    case 0 :
      dev->r = (arg & 0xff0000) >> 16;
      dev->g = (arg & 0xff00) >> 8;
      dev->b =  arg & 0xff;
      return uwn_set_color(dev, dev->r, dev->g, dev->b);

    default:
      printk(KERN_WARNING "uwn_ioctl(): unsupported command %d\n", cmd);
      return -EINVAL;
  }
}

static struct file_operations uwn_file_operations = {
  .open = uwn_open,
  .release = uwn_release,
  .ioctl = uwn_ioctl
};

static struct usb_class_driver uwn_class_driver = {
  .name = "usb/uwn%d",
  .fops = &uwn_file_operations,
  .minor_base = 0
};

/*
** This function will be called when sys entry is read.
*/
static ssize_t
show_color (struct device *dev, struct device_attribute *attr,
	    char *buf)
{
  struct usb_interface *interface;
  struct usb_uwn *mydev;

  interface = to_usb_interface (dev);
  mydev = usb_get_intfdata (interface);

  return sprintf (buf, "%d %d %d\n",  mydev->r, mydev->g, mydev->b);
}

/*
** This function will be called when sys entry is written.
*/
static ssize_t
set_color (struct device *dev, struct device_attribute *attr,
	   const char *buf, size_t count)
{
  struct usb_interface *interface;
  struct usb_uwn *mydev;

  interface = to_usb_interface (dev);
  mydev = usb_get_intfdata (interface);

  sscanf (buf, "%d %d %d", &(mydev->r), &(mydev->g), &(mydev->b));
  (void)uwn_set_color(mydev, mydev->r, mydev->g, mydev->b);

  return count;
}

// This macro generates a struct device_attribute
static DEVICE_ATTR (color, S_IRUGO | S_IWUGO, show_color, set_color);


static int uwn_probe (struct usb_interface *interface, const struct usb_device_id *id)
{
  struct usb_device *udev = interface_to_usbdev (interface);
  struct usb_uwn *uwn_dev;
  int ret;

  printk (KERN_DEBUG "uwn_probe: starting\n");

  ret = usb_register_dev(interface, &uwn_class_driver);
  if (ret < 0) {
    printk (KERN_WARNING "uwn_probe: usb_register_dev() error\n");
    return ret;
  }

  uwn_dev = kmalloc (sizeof(struct usb_uwn), GFP_KERNEL);
  if (uwn_dev == NULL) {
    dev_err (&interface->dev, "Out of memory\n");
    return -ENOMEM;
  }

  // Fill private structure and save it
  memset (uwn_dev, 0, sizeof (*uwn_dev));
  uwn_dev->udev = usb_get_dev(udev);
  usb_set_intfdata (interface, uwn_dev);

  // Create /sys entry
  ret = device_create_file (&interface->dev, &dev_attr_color);
  if (ret < 0) {
    printk (KERN_WARNING "uwn_probe: device_create_file() error\n");
    return ret;
  }

  uwn_dev_init(uwn_dev);

  dev_info(&interface->dev, "USB Webmail Notifier device now attached\n");

  return 0;
}


static void uwn_disconnect(struct usb_interface *interface)
{
  struct usb_uwn *dev;

  dev = usb_get_intfdata(interface);
  usb_deregister_dev(interface, &uwn_class_driver);

  device_remove_file (&interface->dev, &dev_attr_color);
  
  uwn_dev_release(dev);
  usb_set_intfdata(interface, NULL);
  kfree(dev);

  dev_info(&interface->dev, "USB Webmail Notifier now disconnected\n");
}

static struct usb_driver uwn_driver = {
  .name =    "uwn",
  .probe =  uwn_probe,
  .disconnect =  uwn_disconnect,
  .id_table =  id_table,
  //.fops = &uwn_file_operations
};

static int __init usb_uwn_init(void)
{
  int retval = 0;

  printk(KERN_DEBUG "uwn: usb_uwn_init()\n");

  retval = usb_register(&uwn_driver);
  if (retval) {
    err("usb_register failed. Error number %d", retval);
    return retval;
  }

  printk(KERN_DEBUG "uwn: device successfully registered\n");

  return retval;
}

static void __exit usb_uwn_exit(void)
{
  printk(KERN_DEBUG "uwn: usb_uwn_exit()\n");

  usb_deregister(&uwn_driver);
}

module_init (usb_uwn_init);
module_exit (usb_uwn_exit);

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");
