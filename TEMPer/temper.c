/*
 * Linux USB driver for TEMPer (Microdia)
 */
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <asm/uaccess.h>

#define DRIVER_AUTHOR "Pierre Ficheux, pierre.ficheux@gmail.com"
#define DRIVER_DESC "TEMPer driver (Microdia)"


MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
MODULE_LICENSE("GPL");

// Device definition
#define VENDOR_ID	0x0c45
#define PRODUCT_ID	0x7401

#define PERIOD          HZ

// Get temp + init messages
static char uTemp[] = { 0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00 };
static char uIni1[] = { 0x01, 0x82, 0x77, 0x01, 0x00, 0x00, 0x00, 0x00 };
static char uIni2[] = { 0x01, 0x86, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00 };

struct usb_temper {
  struct usb_device *udev;
  int temp;
};

// Forward declaration 
static struct usb_driver temper_driver;

/* Table of devices that work with this driver */
static struct usb_device_id id_table [] = {
  { USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
  { },
};
MODULE_DEVICE_TABLE (usb, id_table);

static void control_msg (struct usb_temper *temper_dev, char umsg[])
{
  int ret = 0;
  char buf[8];

  memcpy(buf, umsg, 8);
  ret = usb_control_msg(temper_dev->udev, usb_sndctrlpipe(temper_dev->udev, 0),
			0x09, 0x21, 0x200, 0x01, buf, 8, 2 * HZ);

  if (ret < 0)
    printk (KERN_WARNING "%s: usb_control_msg(), ret = %d\n", __FUNCTION__, ret);
}

static void interrupt_msg (struct usb_temper *temper_dev)
{
  int ret = 0, i, l;
  char buf[8], msg[256];

  memset(buf, 0, sizeof(buf));
  memset(msg, 0, sizeof(msg));

  ret = usb_interrupt_msg (temper_dev->udev, usb_rcvintpipe (temper_dev->udev, 2), &buf, 8, &l, 2 * HZ);
  if (ret < 0) 
    printk (KERN_WARNING "%s: usb_interrupt_msg(), ret = %d l= %d\n", __FUNCTION__, ret, l);
  else {
    int j = 0;
    for (i = 0 ; i < 8 ; i++) {
      sprintf (&msg[j], "%02x ", buf[i] & 255);
      j += 3;
    }
     
    printk (KERN_INFO "%s\n", msg);
  }
}

static void temper_init (struct usb_temper *temper_dev)
{
  control_msg (temper_dev, uTemp);
  interrupt_msg (temper_dev);
  control_msg (temper_dev, uIni1);
  interrupt_msg (temper_dev);
  control_msg (temper_dev, uIni2);
  interrupt_msg (temper_dev);
}

/* Ask temp value to device  */
static int get_temp_value (struct usb_temper *temper_dev)
{
  int ret = 0, l;
  char buf[8];

  memcpy(buf, uTemp, 8);
  ret = usb_control_msg(temper_dev->udev, usb_sndctrlpipe(temper_dev->udev, 0),
			0x09, 0x21, 0x200, 0x01, buf, 8, 2 * HZ);

  if (ret < 0)
    printk (KERN_WARNING "%s: usb_control_msg(), ret = %d\n", __FUNCTION__, ret);

  memset(buf, 0, sizeof(buf));
  ret = usb_interrupt_msg (temper_dev->udev, usb_rcvintpipe (temper_dev->udev, 2), buf, 8, &l, 2 * HZ);
  if (ret < 0) 
    printk (KERN_WARNING "%s: usb_interrupt_msg(), ret = %d l= %d\n", __FUNCTION__, ret, l);
  else {
    temper_dev->temp = (buf[3] & 0xff) + (buf[2] << 8);
  }

  return ret;
}

static ssize_t show_temp(struct device *dev, struct device_attribute *attr, char *buf)
{
  struct usb_interface *intf = to_usb_interface(dev); 
  struct usb_temper *temper_dev = usb_get_intfdata(intf);

  get_temp_value (temper_dev);

  return sprintf(buf, "%d\n", temper_dev->temp);
}


static ssize_t set_temp(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
  return count;
}
static DEVICE_ATTR(temp, 0444, show_temp, set_temp);

static int temper_open (struct inode *inode, struct file *file)
{
  struct usb_temper *dev;
  struct usb_interface *interface;
  int minor;
  
  minor = iminor(inode);

  // Get interface for device
  interface = usb_find_interface (&temper_driver, minor);
  if (!interface)
    return -ENODEV;

  // Get private data from interface
  dev = usb_get_intfdata (interface);
  if (dev == NULL) {
      printk (KERN_WARNING "temper: can't find device for minor %d\n", minor);
      return -ENODEV;
  }

  // Set to file structure
  file->private_data = (void *)dev;

  return 0;
}

static int temper_release (struct inode *inode, struct file *file)
{
  return 0;
}

static ssize_t temper_read(struct file *file, char *buf, size_t count, loff_t *ppos)
{
  struct usb_temper *dev;
  char buffer[256];
  size_t len;

  memset (buffer, 0, sizeof (buffer));

  /* get the dev object */
  dev = file->private_data;
  if (dev == NULL)
    return -ENODEV;

  /* Get temp and send it to user space */
  get_temp_value (dev);
  len = sprintf(buffer, "%d\n", dev->temp);

  if (*ppos > len)
    return 0;

  len = (count < len - *ppos ? count : len - *ppos);
  if (copy_to_user(buf, &buffer[*ppos], len))
    return -EFAULT;

  (*ppos) += len;

  return len;
}

static struct file_operations temper_fops = {
  .open    = temper_open,
  .release = temper_release,
  .read    = temper_read
};

static struct usb_class_driver temper_class_driver = {
  .name = "usb/temper%d",
  .fops = &temper_fops,
  .minor_base = 0
};

static int temper_probe (struct usb_interface *interface, const struct usb_device_id *id)
{
  struct usb_device *udev = interface_to_usbdev (interface);
  struct usb_temper *temper_dev;
  int ret = 0;

  ret = usb_register_dev(interface, &temper_class_driver);
  if (ret < 0) {
    printk (KERN_WARNING "temper: usb_register_dev() error\n");
    return ret;
  }

  temper_dev = kmalloc (sizeof(struct usb_temper), GFP_KERNEL);
  if (temper_dev == NULL) {
    dev_err (&interface->dev, "Out of memory\n");
    return -ENOMEM;
  }

  // Fill private structure and save it
  memset (temper_dev, 0x00, sizeof (*temper_dev));
  temper_dev->udev = usb_get_dev(udev);
  temper_dev->temp = 0;
  usb_set_intfdata (interface, temper_dev);

  ret = device_create_file(&interface->dev, &dev_attr_temp);
  if (ret < 0) {
    printk (KERN_WARNING "%s: device_create_file() error= %d\n", __FUNCTION__, ret);
    return ret;
  }

  // Needed?
/*
  ret = usb_set_interface  (udev, 1, 0);
  if (ret < 0) {
    printk (KERN_WARNING "%s: usb_set_interface() error= %d\n", __FUNCTION__, ret);
    return ret;
  }
*/
  // Needed?
  //  temper_init (temper_dev);

  dev_info(&interface->dev, "USB TEMPer device now attached\n");

  return 0;
}

static void temper_disconnect(struct usb_interface *interface)
{
  struct usb_temper *dev;

  dev = usb_get_intfdata (interface);
  usb_deregister_dev (interface, &temper_class_driver);
  usb_set_intfdata (interface, NULL);

  device_remove_file(&interface->dev, &dev_attr_temp);
  usb_put_dev(dev->udev);

  kfree(dev);

  dev_info(&interface->dev, "USB TEMer now disconnected\n");
}

static struct usb_driver temper_driver = {
  .name =		"temper",
  .probe =	temper_probe,
  .disconnect =	temper_disconnect,
  .id_table =	id_table,
};

static int __init usb_temper_init(void)
{
  int retval = 0;

  retval = usb_register(&temper_driver);
  if (retval)
    printk(KERN_INFO "usb_register failed. Error number %d", retval);

  return retval;
}

static void __exit usb_temper_exit(void)
{
  usb_deregister(&temper_driver);
}

module_init (usb_temper_init);
module_exit (usb_temper_exit);
