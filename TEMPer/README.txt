Test example :

$ echo "$(cat /sys/bus/usb/drivers/temper/6-2\:1.1/temp) * 125./32000." | bc -l
$ echo "$(cat /dev/temper0) * 125./32000." | bc -l
24.75000000000000000000
